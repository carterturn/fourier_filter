#include "two_part_button.h"
#include <lgl/lgl_primatives.h>

two_part_button::two_part_button(int grid_x, int grid_y, lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_color(color), m_forward_color(color), m_backward_color(color) {
	m_grid_x_min = grid_x;
	m_grid_x_max = grid_x + 1;
	m_grid_y_min = grid_y;
	m_grid_y_max = grid_y + 1;

	m_scale_draw = false;
}

void two_part_button::draw(){
	
	lgl_set_color(m_backward_color.R, m_backward_color.G, m_backward_color.B);
	lgl_draw_rectangle(0.0078125, 0.015625, 0.5 - 0.015625, 1.0 - 0.03125);

	lgl_set_color(m_forward_color.R, m_forward_color.G, m_forward_color.B);
	lgl_draw_rectangle(0.5 + 0.0078125, 0.015625, 0.5 - 0.015625, 1.0 - 0.03125);

	m_text_engine->draw_text("<", 0.125, 0.0625, 0.25, 0.0f, 0.0f, 0.0f);
	m_text_engine->draw_text(">", 0.5 + 0.1875, 0.0625, 0.25, 0.0f, 0.0f, 0.0f);
}

void two_part_button::on_hover_start(float x, float y){
	if(x > 0.5f){
		m_forward_color.R = m_color.R * 0.75f;
		m_forward_color.G = m_color.G * 0.75f;
		m_forward_color.B = m_color.B * 0.75f;

		m_backward_color = m_color;
	}
	else if(x < 0.5f){
		m_backward_color.R = m_color.R * 0.75f;
		m_backward_color.G = m_color.G * 0.75f;
		m_backward_color.B = m_color.B * 0.75f;

		m_forward_color = m_color;
	}
}

void two_part_button::on_left_mouse_press(float x, float y){
	if(x > 0.5f){
		on_forward_press();
	}
	else if(x < 0.5f){
		on_backward_press();
	}
}

void two_part_button::on_left_mouse_release(float x, float y){}
void two_part_button::on_right_mouse_press(float x, float y){}
void two_part_button::on_right_mouse_release(float x, float y){}

void two_part_button::on_hover_end(){
	m_forward_color = m_color;
	m_backward_color = m_color;
}

void two_part_button::on_forward_press(){}

void two_part_button::on_backward_press(){}
