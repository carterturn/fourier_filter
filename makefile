CPP=g++ -g
CFLAGS=-llgl -llgl_widgets -lGL -lglfw -lfftw3
build:
	$(CPP) fourier_filter.cpp graph_object.cpp two_part_button.cpp string_util.cpp -o fourier_filter $(CFLAGS)
