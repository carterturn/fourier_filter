#include <string>
#include <vector>

/**
   A collection of basic string utilities
 */

/**
   Splits a string into a vector of parts on the character splitter
 */
std::vector<std::string> split_string(std::string data, char splitter);
/**
   Int to string
 */
std::string its(int in);
/**
   Removes tabs and spaces at the beginning and end of strings
 */
std::string trim_whitespace(std::string data);
