#pragma once

#include <vector>

#include <lgl/lgl_object.h>

using std::vector;

struct graph_object_cursor {
	int index;
	lgl_color color;
};

class graph_object : public lgl_object {
public:
	graph_object(float x, float y, float width, float height, lgl_color color, lgl_text * text_engine);

	void draw() override;

	void set_data(vector<double> data);
	void set_bounds(int start_index, int end_index);

	void add_cursor(int index, lgl_color color);
	double value_at_index(int index);

protected:
	vector<double> m_data;
	int m_start_index, m_end_index;

	vector<graph_object_cursor> m_cursors;

	lgl_color m_color;
};
