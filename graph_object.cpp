#include "graph_object.h"

#include <GL/gl.h>
#include <lgl/lgl_primatives.h>

#include <algorithm>
#include <cmath>

using std::max;
using std::min;
using std::max_element;
using std::min_element;

graph_object::graph_object(float x, float y, float width, float height, lgl_color color, lgl_text * text_engine)
	: lgl_object(text_engine), m_color(color) {
	m_scale_draw = true;

	m_grid_x_min = x;
	m_grid_y_min = y;
	m_grid_x_max = x + width;
	m_grid_y_max = y + height;
}

void graph_object::draw(){
	lgl_set_color(0.1f, 0.1f, 0.1f);

	lgl_draw_rectangle(0, 0, 1, 1);

	if(m_data.size() == 0){
		return;
	}

	float x_step = 1.0f / (float) (m_end_index - m_start_index);

	double y_min = *min_element(m_data.begin() + m_start_index, m_data.begin() + m_end_index);
	double y_max = *max_element(m_data.begin() + m_start_index, m_data.begin() + m_end_index);
	float y_scale = 1.0f / (y_max - y_min);

	lgl_set_color(m_color.R, m_color.G, m_color.B);

	glBegin(GL_POINTS);

	for(int i = m_start_index; i < m_end_index; i++){
		glVertex2f(x_step * (float) (i - m_start_index), (float) (m_data[i] - y_min) * y_scale);
	}

	glEnd();

	glBegin(GL_LINES);

	for(graph_object_cursor goc : m_cursors){
		lgl_set_color(goc.color.R, goc.color.G, goc.color.B);

		glVertex2f(x_step * (float) goc.index, 0);
		glVertex2f(x_step * (float) goc.index, 1);
	}

	glEnd();

	m_cursors.clear();
}

void graph_object::set_data(vector<double> data){
	m_data = data;

	m_start_index = 0;
	m_end_index = m_data.size();
}

void graph_object::set_bounds(int start_index, int end_index){
	if(m_end_index < m_start_index){
		return;
	}

	m_start_index = max(0, start_index);
	m_end_index = min(end_index, (int) m_data.size());
}

void graph_object::add_cursor(int index, lgl_color color){
	m_cursors.push_back({index, color});
}

double graph_object::value_at_index(int index){
	if(index > m_data.size() + m_start_index){
		return 0.0;
	}
	return m_data[m_start_index + index];
}
