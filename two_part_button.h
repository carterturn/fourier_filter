#pragma once

#include <lgl/lgl_object.h>

using std::string;

class two_part_button : public lgl_object {
public:
	two_part_button(int grid_x, int grid_y, lgl_color color, lgl_text * text_engine);

	void draw();

	void on_hover_start(float x, float y);
	void on_left_mouse_press(float x, float y);
	void on_left_mouse_release(float x, float y);
	void on_right_mouse_press(float x, float y);
	void on_right_mouse_release(float x, float y);
	void on_hover_end();

	virtual void on_forward_press();
	virtual void on_backward_press();

protected:
	lgl_color m_color;

	lgl_color m_forward_color;
	lgl_color m_backward_color;
};
