#include <chrono>
#include <cmath>
#include <iostream>
#include <fstream>
#include <thread>

#include <fftw3.h>

#include <lgl/lgl_button.h>
#include <lgl/lgl_edit_indicator.h>
#include <lgl/lgl_elbow.h>
#include <lgl/lgl_color_dim.h>
#include <lgl/lgl_color_list.h>
#include <lgl/lgl_indicator.h>
#include <lgl/lgl_text.h>
#include <lgl/lgl_window.h>

#include <lgl/widgets/lgl_file_open.h>

#include "graph_object.h"
#include "two_part_button.h"
#include "string_util.h"

using namespace std;
using namespace std::chrono;
using std::this_thread::sleep_for;

#define FONT_FILE "monospace.bmp"

class filter_limit_button : public two_part_button {
public:
	filter_limit_button(int * filter_limit, int minimum, int maximum,
						int grid_x, int grid_y, lgl_color color, lgl_text * text_engine)
		: two_part_button(grid_x, grid_y, color, text_engine), m_filter_limit(filter_limit),
		  m_minimum(minimum), m_maximum(maximum) {
		m_forward = false;
		m_backward = false;
	}

	void on_forward_press() override{
		m_forward = true;
	}

	void on_backward_press() override{
		m_backward = true;
	}

	void on_left_mouse_release(float x, float y) override{
		m_forward = false;
		m_backward = false;
	}

	bool update_filter(){
		if(m_forward){
			*m_filter_limit += 10;
			if(*m_filter_limit > m_maximum){
				*m_filter_limit = m_maximum;
			}
			return true;
		}
		else if(m_backward){
			*m_filter_limit -= 10;
			if(*m_filter_limit < m_minimum){
				*m_filter_limit = m_minimum;
			}
			return true;
		}

		return false;
	}

protected:
	int * m_filter_limit;
	int m_minimum, m_maximum;
	bool m_forward, m_backward;
};

class filter_edit_indicator : public lgl_edit_indicator {
public:
	filter_edit_indicator(int * filter_limit, int minimum, int maximum,
						  int grid_x, int grid_y, lgl_color color, lgl_text * text_engine)
		: lgl_edit_indicator(grid_x, grid_y, 1, color, text_engine), m_filter_limit(filter_limit),
		  m_minimum(minimum), m_maximum(maximum), m_update(false) {}

	void on_edit_complete() override{
		*m_filter_limit = atoi(m_text.c_str());
		if(*m_filter_limit > m_maximum){
			*m_filter_limit = m_maximum;
		}
		else if(*m_filter_limit < m_minimum){
			*m_filter_limit = m_minimum;
		}
		m_update = true;
	}

	bool updated(){
		if(m_update){
			m_update = false;
			return true;
		}
		return false;
	}

	void on_edit_cancel() override{
		m_text = to_string(*m_filter_limit);
	}

protected:
	int m_minimum, m_maximum;
	int * m_filter_limit;
	bool m_update;
};

class trigger_button : public lgl_button_color_dim {
public:
	trigger_button(bool * triggered, int grid_x, int grid_y, string text, lgl_color color, lgl_text * text_engine)
		: lgl_button_color_dim(grid_x, grid_y, 1, text, color, text_engine), m_triggered(triggered){}

	void on_left_mouse_press(){
		*m_triggered = true;
	}

protected:
	bool * m_triggered;
};

vector<double> load_data(){
	lgl_file_open file_open_dialog(FONT_FILE);

	while(!file_open_dialog.got_file()){
		file_open_dialog.get_file();
	}

	ifstream input_data_file(file_open_dialog.get_filename());

	if(!input_data_file.is_open()){
		cout << "Could not open\n";
		return load_data();
		// This recursion could cause an issue if the user loops through too many times, but that is unlikely
	}

	{
		string header;
		getline(input_data_file, header);
	}

	vector<double> input_data;

	while(!input_data_file.eof()){
		string line;
		getline(input_data_file, line);
		vector<string> entries = split_string(line, ',');
		input_data.push_back(strtod(entries[0].c_str(), NULL));
	}

	input_data_file.close();

	return input_data;
}

void save_data(double * data, int length){
	lgl_file_open file_open_dialog(FONT_FILE);

	file_open_dialog.get_file();

	if(!file_open_dialog.got_file()){
		cout << "No file" << "\n";
		return;
	}

	ofstream input_data_file(file_open_dialog.get_filename());

	if(!input_data_file.is_open()){
		cout << "Could not open\n";
		save_data(data, length);
		return;
		// This recursion could cause an issue if the user loops through too many times, but that is unlikely
	}

	for(int i = 0; i < length; i++){
		input_data_file << data[i] << "\n";
	}

	input_data_file.close();

	return;
}
	

vector<double> perform_fft(double * data, int length, double * fft){
	bool cleanup = false;
	if(fft == NULL){
		fft = new double[length];
		cleanup = true;
	}

	fftw_plan fft_plan = fftw_plan_r2r_1d(length, data, fft, FFTW_R2HC, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);

	fftw_execute(fft_plan);

	fftw_destroy_plan(fft_plan); // Efficiency is irrelevant

	vector<double> fft_vector;

	fft_vector.push_back(abs(fft[0]));
    for(int i = 1; i < length / 2; i++){
        fft_vector.push_back(sqrt(fft[i]*fft[i] + fft[length - i]*fft[length - i]));
    }

	if(cleanup){
		delete[] fft;
	}

	return fft_vector;
}

vector<double> invert_fft(double * fft, int length){
	double * data = new double[length];

	fftw_plan fft_plan = fftw_plan_r2r_1d(length, fft, data, FFTW_HC2R, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);

	fftw_execute(fft_plan);

	fftw_destroy_plan(fft_plan); // Efficiency still irrelevant

	vector<double> data_vector;

	for(int i = 0; i < length; i++){
		data_vector.push_back(data[i]);
	}

	delete[] data;

	return data_vector;
}

int main(int argc, char * argv[]){
	vector<double> data = load_data();

	lgl_window window("Fourier Filter", 14, 16);

	if(window.initialize() != 0){
		cout << "Error building window\n";
		return -1;
	}

	lgl_text text_engine(FONT_FILE);
	text_engine.initialize();

	int input_upper_bound = data.size()-1;
	int input_lower_bound = 0;
	filter_limit_button upper_input_control(&input_upper_bound, 0, data.size()-1, 0, 15, ERA_4_3, &text_engine);
	filter_edit_indicator upper_input_edit(&input_upper_bound, 0, data.size()-1, 1, 15, ERA_4_3, &text_engine);
	upper_input_edit.set_text(to_string(input_upper_bound));
	filter_limit_button lower_input_control(&input_lower_bound, 0, data.size()-1, 0, 14, ERA_4_3, &text_engine);
	filter_edit_indicator lower_input_edit(&input_lower_bound, 0, data.size()-1, 1, 14, ERA_4_3, &text_engine);
	lower_input_edit.set_text(to_string(input_lower_bound));
	lgl_elbow input_wave_elbow(0, 11, 1, 12, lgl_elbow::ORIENT_BOTTOM_LEFT,
							   "Input Wave", ERA_4_3, &text_engine);

	int fft_lower_bound = 0;
	int fft_upper_bound = data.size()/2;
	lgl_elbow fourier_transform_upper_elbow(0, 9, 0, 12, lgl_elbow::ORIENT_TOP_LEFT,
											"Fourier", ERA_4_5, &text_engine);
	filter_limit_button upper_filter_control(&fft_upper_bound, 0, data.size()/2-1, 0, 8, ERA_4_5, &text_engine);
	filter_edit_indicator upper_filter_edit(&fft_upper_bound, 0, data.size()-1, 1, 8, ERA_4_5, &text_engine);
	upper_filter_edit.set_text(to_string(fft_upper_bound));
	filter_limit_button lower_filter_control(&fft_lower_bound, 0, data.size()/2-1, 0, 7, ERA_4_5, &text_engine);
	filter_edit_indicator lower_filter_edit(&fft_lower_bound, 0, data.size()-1, 1, 7, ERA_4_5, &text_engine);
	lower_filter_edit.set_text(to_string(fft_lower_bound));
	lgl_elbow fourier_transform_lower_elbow(0, 5, 0, 12, lgl_elbow::ORIENT_BOTTOM_LEFT,
											"", ERA_4_5, &text_engine);

	lgl_elbow filtered_wave_elbow(0, 1, 2, 12, lgl_elbow::ORIENT_TOP_LEFT,
								  "Filtered", ERA_4_4, &text_engine);
	bool save_filtered = false;
	trigger_button save_filtered_wave(&save_filtered, 0, 0, "Save", ERA_4_4, &text_engine);

	window.add_object(&upper_input_control);
	window.add_object(&upper_input_edit);
	window.add_object(&lower_input_control);
	window.add_object(&lower_input_edit);
	window.add_object(&input_wave_elbow);

	window.add_object(&fourier_transform_upper_elbow);
	window.add_object(&upper_filter_control);
	window.add_object(&upper_filter_edit);
	window.add_object(&lower_filter_control);
	window.add_object(&lower_filter_edit);
	window.add_object(&fourier_transform_lower_elbow);

	window.add_object(&filtered_wave_elbow);
	window.add_object(&save_filtered_wave);

	float graph_length = 12.5f;

	graph_object input_graph(2.25f, 11.5f, graph_length, 4.5f, {1.0f, 1.0f, 1.0f}, &text_engine);
	graph_object fourier_transform(2.25f, 5.75f, graph_length, 4.5f, {1.0f, 1.0f, 1.0f}, &text_engine);
	graph_object filtered_graph(2.25f, 0, graph_length, 4.5f, {1.0f, 1.0f, 1.0f}, &text_engine);

	window.add_object(&input_graph);
	window.add_object(&fourier_transform);
	window.add_object(&filtered_graph);

	input_graph.set_data(data);

	int data_length = data.size();

	double * fft_data = new double[data_length];
	double * fft_filtered_data = new double[data_length];
	fourier_transform.set_data(perform_fft(data.data(), data_length, fft_data));
	fourier_transform.set_bounds(1, data_length / 2);

	vector<double> filtered_data;

	filtered_graph.set_data(invert_fft(fft_data, data_length));
	filtered_graph.set_bounds(0, data_length);

	bool reload_filtered = true;

	while(window.keep_running()){

		window.update();

		if(upper_input_control.update_filter() || lower_input_control.update_filter()
		   || upper_input_edit.updated() || lower_input_edit.updated()){
			data_length = input_upper_bound - input_lower_bound;

			input_graph.set_bounds(input_lower_bound, input_upper_bound);
			upper_input_edit.set_text(to_string(input_upper_bound));
			lower_input_edit.set_text(to_string(input_lower_bound));

			delete[] fft_data;
			fft_data = new double[data_length];

			delete[] fft_filtered_data;
			fft_filtered_data = new double[data_length];

			fourier_transform.set_data(perform_fft(data.data() + input_lower_bound, data_length, fft_data));
			fft_lower_bound = 1; // Hide DC component
			fft_upper_bound = data_length / 2;
			fourier_transform.set_bounds(fft_lower_bound, fft_upper_bound);

			reload_filtered = true;
		}

		if(upper_filter_control.update_filter() || lower_filter_control.update_filter()
		   || upper_filter_edit.updated() || lower_filter_edit.updated()){
			reload_filtered = true;

			upper_filter_edit.set_text(to_string(fft_upper_bound));
			lower_filter_edit.set_text(to_string(fft_lower_bound));
		}

		fourier_transform.add_cursor(fft_lower_bound, {0.0f, 1.0f, 0.0f});
		fourier_transform.add_cursor(fft_upper_bound, {0.0f, 1.0f, 0.0f});

		if(reload_filtered){
			fft_filtered_data[0] = fft_data[0];
			for(int i = 0; i < fft_lower_bound; i++){
				fft_filtered_data[i] = 0;
			}
			for(int i = fft_lower_bound; i < fft_upper_bound; i++){
				fft_filtered_data[i] = fft_data[i];
				fft_filtered_data[data_length - 1 - i] = fft_data[data_length - 1 - i];
			}
			for(int i = fft_upper_bound; i < data_length/2; i++){
				fft_filtered_data[i] = 0;
			}

			filtered_data = invert_fft(fft_filtered_data, data_length);
			filtered_graph.set_data(filtered_data);
			filtered_graph.set_bounds(0, data_length);

			reload_filtered = false;
		}

		if(save_filtered){
			save_data(filtered_data.data(), filtered_data.size());
			save_filtered = false;
		}

		sleep_for(milliseconds(10));
	}

	delete[] fft_data;
	delete[] fft_filtered_data;

	return 0;
}
